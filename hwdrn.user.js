// ==UserScript==
// @name        betting5: HW date redirector and navigator
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting5-hw-date-redirector-navigator
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting5-hw-date-redirector-navigator/raw/master/hwdrn.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting5-hw-date-redirector-navigator/raw/master/hwdrn.user.js
// @match       *://*.hintwise.com/*
// @run-at      document-start
// @version     1.0.3
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;
var urlLastPart = url.substr(url.lastIndexOf('/') + 1);


// HELPERS

function redirectToDate(url, delta = 0) {
    var parts = [];
    if (/date/.test(url)) {
        parts = urlLastPart.split(/[-=&]+/);
        parts[3] = Number(parts[3]) + delta;
        if (parts[3] < 10) {parts[3] = '0' + parts[3]};
    } else {
        var today = new Date();
        parts[3] = today.getDate();
        if (parts[3] < 10) {parts[3] = '0' + parts[3]};
        parts[2] = today.getMonth() + 1; //January is 0!
        if (parts[2] < 10) {parts[2] = '0' + parts[2]};
        parts[1] = today.getFullYear();
    }

    var datePage = parts[1] + '-' + parts[2] + '-' + parts[3];
    return window.location.origin + '/?date=' + datePage + '&show=all'
}

// MAIN

// Immediately redirect to today if an url doesn't contain a link to a date
if (url == window.location.protocol + '//' + window.location.hostname + '/') {
    window.location.replace(redirectToDate(url));
}

// Add hotkeys to navigate tips pages back an forth
addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/)) {
        return;
    }
    e.preventDefault();

    if (e.which == 74) {
        window.location.replace(redirectToDate(url, -1));
    } else if (e.which == 75) {
        window.location.replace(redirectToDate(url, +1));
    }
})
